package br.com.ia.oct.mcd.common.exceptions;

import javax.xml.ws.WebFault;

@WebFault
public class OCTMcDRuntimeException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public OCTMcDRuntimeException (String msg) {
		super(msg);
	}
	
	public OCTMcDRuntimeException (String msg, Throwable cause) {
		super(msg, cause);
	}

}