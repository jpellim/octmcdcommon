package br.com.ia.oct.mcd.common.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IA_MCDONALDS_V01_MAPEAMENTO_AT")
public class McDMapping implements Serializable {
	
	@Id
	private String Request_ID;
	
	@Enumerated(EnumType.STRING)
	private McDMappingTipoMap Tipo_de_categorizacao;
	
	private String Tier_1;
	private String Tier_2;
	private String Tier_3;
	private String First_Name;
	
	private String Categoria;
	private String ccti_item;
	private String ccti_type;	
	private String Last_Name2;
	
		
	//GET
	
	public String getRequest_ID(){
		return Request_ID;
	}
	
	public McDMappingTipoMap getTipo_de_categorizacao(){
		return Tipo_de_categorizacao;
	}	
	
	public String getTier_1(){
		return Tier_1;
	}
	
	public String getTier_2(){
		return Tier_2;
	}
	
	public String getTier_3(){
		return Tier_3;
	}
	
	public String getFirst_name(){
		return First_Name;
	}
	
	public String getCategoria(){
		return Categoria;
	}
	
	public String getccti_item(){
		return ccti_item;
	}
	
	public String getccti_type(){
		return ccti_type;
	}
	
	public String getLast_Name2(){
		return Last_Name2;
	}
	
	//SET
	
	public void setRequest_ID (String request_id){
		Request_ID = request_id;
	}
	
	public void setTipo_de_categorizacao(McDMappingTipoMap tipo_de_categorizacao){
		Tipo_de_categorizacao = tipo_de_categorizacao;
	}
	
	public void setTier_1(String tier_1){
		Tier_1 = tier_1;
	}
	
	public void setTier_2(String tier_2){
		Tier_2 = tier_2;
	}
	
	public void setTier_3(String tier_3){
		Tier_3 = tier_3;
	}
	
	public void setFirst_name(String first_Name){
		First_Name = first_Name;
	}
	
	public void setCategoria(String categoria){
		Categoria = categoria;
	}
	
	public void setccti_item(String Ccti_item){
		ccti_item = Ccti_item;
	}
	
	public void setccti_type(String Ccti_type){
		ccti_type = Ccti_type;
	}
	
	public void setLast_Name2(String last_Name2){
		Last_Name2 = last_Name2;
	}
	

}
