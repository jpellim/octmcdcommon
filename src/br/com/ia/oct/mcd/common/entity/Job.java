package br.com.ia.oct.mcd.common.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_OCT_MCD_JOB_DEFINITIONS")
public class Job implements Serializable {
	@Id
	@GeneratedValue	
	private Long   id;
	private String produto;
	private String uid;
	private String nome;
	private String descricao;
	private String jndiLookup;
	private String clazz;
	private String cronTrigger;
	private String grupo;
	private String node;
	@Column(columnDefinition="tinyint")
	private Boolean ativo;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar lastExecution;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar nextExecution;
	private Long lastExecutionTime;
	private String lastExecutionStatus;
	private String lastErrorMessage;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getJndiLookup() {
		return jndiLookup;
	}
	public void setJndiLookup(String jndiLookup) {
		this.jndiLookup = jndiLookup;
	}
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	public String getCronTrigger() {
		return cronTrigger;
	}
	public void setCronTrigger(String cronTrigger) {
		this.cronTrigger = cronTrigger;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
	public Calendar getLastExecution() {
		return lastExecution;
	}
	public void setLastExecution(Calendar lastExecution) {
		this.lastExecution = lastExecution;
	}
	public Calendar getNextExecution() {
		return nextExecution;
	}
	public void setNextExecution(Calendar nextExecution) {
		this.nextExecution = nextExecution;
	}
	public Long getLastExecutionTime() {
		return lastExecutionTime;
	}
	public void setLastExecutionTime(Long lastExecutionTime) {
		this.lastExecutionTime = lastExecutionTime;
	}
	public String getLastExecutionStatus() {
		return lastExecutionStatus;
	}
	public void setLastExecutionStatus(String lastExecutionStatus) {
		this.lastExecutionStatus = lastExecutionStatus;
	}
	public String getLastErrorMessage() {
		return lastErrorMessage;
	}
	public void setLastErrorMessage(String lastErrorMessage) {
		this.lastErrorMessage = lastErrorMessage;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
}
