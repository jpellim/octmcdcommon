package br.com.ia.oct.mcd.common.entity.remedy;

import java.io.Serializable;


public class RemedyAttachment implements Serializable{
	
	private static final long serialVersionUID = 45L;

	private byte[] byteArray;
	private String name;
	private int size;
	
	public byte[] getByteArray() {
		return byteArray;
	}
	public void setByteArray(byte[] byteArray) {
		this.byteArray = byteArray;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
}
