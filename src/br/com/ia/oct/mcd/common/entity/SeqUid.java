package br.com.ia.oct.mcd.common.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_OCT_MCD_SEQUENCE_UID")
public class SeqUid implements Serializable {
	@Id
	@GeneratedValue	
	private Long   id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Calendar getData() {
		return data;
	}
	
	public void setData(Calendar data) {
		this.data = data;
	}
}
