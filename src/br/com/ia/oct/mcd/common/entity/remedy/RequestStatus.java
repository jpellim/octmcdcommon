package br.com.ia.oct.mcd.common.entity.remedy;

import java.io.Serializable;

import br.com.ia.oct.mcd.common.annotations.EntityProp;
import br.com.ia.oct.mcd.common.annotations.FieldProp;

@EntityProp(formName="IA:PRJ_13467_16:CompanyStatus")
public class RequestStatus implements Serializable {
	@FieldProp(fieldID=536870913)
	private String companyIN;
	@FieldProp(fieldID=536870915)
	private String companyOUT;
	@FieldProp(fieldID=536870916)
	private String statusIN;
	@FieldProp(fieldID=536870917)
	private String statusOUT;
	
	public String getCompanyIN() {
		return companyIN;
	}
	public void setCompanyIN(String companyIN) {
		this.companyIN = companyIN;
	}
	public String getCompanyOUT() {
		return companyOUT;
	}
	public void setCompanyOUT(String companyOUT) {
		this.companyOUT = companyOUT;
	}
	public String getStatusIN() {
		return statusIN;
	}
	public void setStatusIN(String statusIN) {
		this.statusIN = statusIN;
	}
	public String getStatusOUT() {
		return statusOUT;
	}
	public void setStatusOUT(String statusOUT) {
		this.statusOUT = statusOUT;
	}
}
