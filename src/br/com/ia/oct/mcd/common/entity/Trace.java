package br.com.ia.oct.mcd.common.entity;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_OCT_MCD_TRACE")
public class Trace implements Serializable {
	@Id
	@GeneratedValue	
	private Long id;

	private Long seqUid;
	private String componentID;
	
	@Column(length=1000)
	private String message;
	
	@Lob
	private String details;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar date;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getSeqUid() {
		return seqUid;
	}
	
	public void setSeqUid(Long seqUid) {
		this.seqUid = seqUid;
	}
	
	public String getComponentID() {
		return componentID;
	}
	
	public void setComponentID(String componentID) {
		this.componentID = componentID;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getDetails() {
		return details;
	}
	
	public void setDetails(String details) {
		this.details = details;
	}
	
	public Calendar getDate() {
		return date;
	}
	
	public void setDate(Calendar date) {
		this.date = date;
	}
}
