package br.com.ia.oct.mcd.common.entity.remedy;

import java.io.Serializable;

import br.com.ia.oct.mcd.common.annotations.EntityProp;
import br.com.ia.oct.mcd.common.annotations.FieldProp;

@EntityProp(formName="IA:PRJ_13467_16:Categ_Map")
public class Categorization implements Serializable {
	@FieldProp(fieldID=536870913)
	private String tipo;
	
	@FieldProp(fieldID=536870914)
	private String companyIN;
	
	@FieldProp(fieldID=536870915)
	private String companyOUT;
	
	@FieldProp(fieldID=536870916)
	private String categoryIN;
	
	@FieldProp(fieldID=536870917)
	private String categoryOUT;
	
	@FieldProp(fieldID=536870918)
	private String grupoSolucionadorOUT;
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getCompanyIN() {
		return companyIN;
	}
	
	public void setCompanyIN(String companyIN) {
		this.companyIN = companyIN;
	}
	
	public String getCompanyOUT() {
		return companyOUT;
	}
	
	public void setCompanyOUT(String companyOUT) {
		this.companyOUT = companyOUT;
	}
	
	public String getCategoryIN() {
		return categoryIN;
	}
	
	public void setCategoryIN(String categoryIN) {
		this.categoryIN = categoryIN;
	}
	
	public String getCategoryOUT() {
		return categoryOUT;
	}
	
	public void setCategoryOUT(String categoryOUT) {
		this.categoryOUT = categoryOUT;
	}
	
	public String getGrupoSolucionadorOUT() {
		return grupoSolucionadorOUT;
	}
	
	public void setGrupoSolucionadorOUT(String grupoSolucionadorOUT) {
		this.grupoSolucionadorOUT = grupoSolucionadorOUT;
	}
}
