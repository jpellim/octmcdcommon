package br.com.ia.oct.mcd.common.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_OCT_MCD_COMPONENT_DEFINITIONS")
public class Component implements Serializable {
	@Id
	@GeneratedValue	
	private Long   id;
	private String produto;
	private String uid;
	private String cVersion;
	private String nome;
	private String descricao;
	private String jndiLookup;
	private String clazz;
	private String cronTrigger;
	private String grupo;
	private String node;
	@Column(columnDefinition="tinyint")
	private Boolean ativo;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar lastPingTime;
	private Long	 duration;
	private String   status;
	private String   lastErrorMessage;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getJndiLookup() {
		return jndiLookup;
	}
	public void setJndiLookup(String jndiLookup) {
		this.jndiLookup = jndiLookup;
	}
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	public String getCronTrigger() {
		return cronTrigger;
	}
	public void setCronTrigger(String cronTrigger) {
		this.cronTrigger = cronTrigger;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public Calendar getLastPingTime() {
		return lastPingTime;
	}
	public void setLastPingTime(Calendar lastPingTime) {
		this.lastPingTime = lastPingTime;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLastErrorMessage() {
		return lastErrorMessage;
	}
	public void setLastErrorMessage(String lastErrorMessage) {
		this.lastErrorMessage = lastErrorMessage;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getcVersion() {
		return cVersion;
	}
	public void setcVersion(String cVersion) {
		this.cVersion = cVersion;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
}
