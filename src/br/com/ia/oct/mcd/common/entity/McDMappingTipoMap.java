package br.com.ia.oct.mcd.common.entity;

public enum McDMappingTipoMap {
	Cat("Categorizacao"),
	Cod_Designicao("Cod Designacao"),
	Impacto("Impact"),
	Urgencia("Urgency"),
	Cat_Res("Cat Resolucao"),
	Grp_Atd("Grupo Atendimento");
	
	private String name;
	
	McDMappingTipoMap (String p_McD_Mapping){
		name = p_McD_Mapping;
	}
	
	public String toString(){
		return name;
	}
	
	public McDMappingTipoMap fromString(String value){
		String eval = value.trim().toLowerCase();
		McDMappingTipoMap ret = null;
		
		switch (eval) {
			case "categorizacao":
					ret = Cat;
					break;
			case "cod designacao":
			case "cod designação":
					ret = Cod_Designicao;
					break;
			case "impact":
					ret = Impacto;
					break;
			case "urgencia":
					ret = Urgencia;
					break;
			case "cat resolucao":
			case "cat resolução":
					ret = Cat_Res;
					break;
			case "grupo atendimento":
					ret = Grp_Atd;
					break;
			default:
					break;
		}
		
		return ret;
		
	}
	
	
	
}
