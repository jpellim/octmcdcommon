package br.com.ia.oct.mcd.common.math;

public class Math {

	private Math obj;
	
	public Math getObj() {
		return obj;
	}

	public static boolean betweenExclusive(int x, int min, int max) {
		return x > min && x < max;    
	}

	public static boolean betweenInclusive(int x, int min, int max) {
		return x >= min && x <= max;    
	}

}
