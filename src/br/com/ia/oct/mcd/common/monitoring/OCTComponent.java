package br.com.ia.oct.mcd.common.monitoring;

/**
 * Interface que define que o componente faz parte do sistema OCT
 * @author fernandosapata
 *
 */
public interface OCTComponent {
	public Boolean isAlive ();
}
