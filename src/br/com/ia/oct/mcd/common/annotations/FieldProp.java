package br.com.ia.oct.mcd.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FieldProp {

	public int fieldID()   			default 0;
	public int maxLength() 			default 0;
	
	public String defaultValue()	default "";
	public String codErro()			default "";
	public String msgErro()			default "";
	
	public boolean isDate()			default false;
	public boolean obrigatorio()	default false;
	public boolean isFile()			default false;
	
}