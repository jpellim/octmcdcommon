package br.com.ia.oct.mcd.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Anota��o utilizada para passar os par�metros necess�rios para o registro de um job na plataforma
 * @author fernandosapata
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OCTJob {
	String produto() default "OCT-MCD";
	
	String uid();
	
	String nome();

	String clazz();

	String descricao();

	String jndiLookup();

	boolean ativo();

	String cronTrigger();

	String grupo();
	
	String node();

}
