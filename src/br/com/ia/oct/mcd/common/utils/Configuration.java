package br.com.ia.oct.mcd.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * Classe utilizada para gerenciamento dos par�metros do sistema McD.
 * Este componente Utiliza inicializa��o seletiva, caso encontre o arquivo oct-mcd.properties no diret�rio conf do 
 * servidor default no JBOSS utiliza este arquivo, caso contr�rio utiliza o mesmo arquivo dentro do classPath
 * @author fernandosapata
 *
 */
public class Configuration {
	private static final Logger LOG = Logger.getLogger(Configuration.class);
	private static ResourceBundle resourceBundle;
	private static Properties properties;
	private static Boolean jbossConf = false;
	
	static {
		loadBundle();
	}
	
	public static void loadBundle () {
		try {
			String path = System.getProperty("jboss.server.config.url") + "oct-mcd.properties";
			path = path.replaceAll("file:", "");
			
			LOG.info("PATH: " + path);
			
			if(new File(path).exists()) {
				properties = new Properties();
				properties.load(new FileInputStream(path));  

				jbossConf = true;
				
				LOG.info("Loaded application properties from file: " + path);  
			} else {  
				resourceBundle = ResourceBundle.getBundle("oct-mcd");
				LOG.info("Loaded application properties from classpath...");				
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}		
	}
	
	public static String getConfig (String key) {
		if (jbossConf) {
			return properties.getProperty(key);
		} else {
			return resourceBundle.getString(key);
		}
	}
	
	public static String getFormatedConfig(String key, Object[] obj) {
		if (jbossConf) {
			String msg = MessageFormat.format(properties.getProperty(key), obj);
			return msg;
		} else {
			String msg = MessageFormat.format(resourceBundle.getString(key), obj);
			return msg;
		}		
	}
}