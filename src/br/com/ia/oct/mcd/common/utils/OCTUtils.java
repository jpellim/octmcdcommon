package br.com.ia.oct.mcd.common.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.UUID;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;



/**
 * Classe com m�todos utilit�rios para p produto OCT-MCD
 * @author fernandosapata
 *
 */
public class OCTUtils {
	private static final Logger LOG = Logger.getLogger(OCTUtils.class);
	
	/**
	 * M�todo utilizado para retornar o hostname da m�quina em que o OCT-MCD est� rodando.
	 * Utilizado como base para decidir se o JOB dever� ser iniciado neste determinado host
	 * @return
	 */
	public static String getHostname () {
		try {
		    String result = InetAddress.getLocalHost().getHostName();
		    if (StringUtils.isNotEmpty( result)) {
		    	LOG.info("Retornando hostname [" + result + "]");
		        return result;
		    }
		} catch (UnknownHostException e) {
		    // failed;  try alternate means.
		}

		String host = System.getenv("COMPUTERNAME");
		if (host != null) {
			LOG.info("Retornando hostname [" + host + "]");
		    return host;
		}
		host = System.getenv("HOSTNAME");
		if (host != null){
			LOG.info("Retornando hostname [" + host + "]");
		    return host;
		}
		
		// undetermined.
		return null;		
	}
	
	/**
	 * M�todo utilizado para converter as datas retornadas pelo Remedy no formato esperado pelos Services da VPI
	 * @param obj
	 * @return
	 */
	public static XMLGregorianCalendar convertTimestampToXmlGregorianCalendar (Object obj) {
		try {
//			Timestamp timestamp = null;
			Date date= null;
			
			if (obj == null) {
				// retornar NULL quebra o XML
				// return "";
				LOG.error("Parametro [obj=" + obj + "] em OCTUtils.convertTimestampToXmlGregorianCalendar informado é nulo!");
				date = new Date(1);
			}
			
			if (obj instanceof String) {
				if ("".equalsIgnoreCase((String) obj)) {
					return DatatypeFactory.newInstance().newXMLGregorianCalendar();
				}
				
				String tmp = (String) obj;
				tmp = tmp.replaceAll("\\[Timestamp\\=", "");
				tmp = tmp.replaceAll("\\]", "");				
				
				date = new Date(Long.valueOf(tmp) * 1000);
			}
			if (obj instanceof Long) {
				date = new Date((Long) obj * 1000);
			}
			
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			
			XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			
			return calendar;
		} catch (Exception e) {
			LOG.error("Erro convertendo [obj=" + obj + "] em XMLGregorianCalendar [" + e.getMessage() + "]", e);
			return null;
		}
	}
	
	public static String getRandomString(int p_size, boolean p_with_symbols) throws Exception {
		String letters   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String numbers   = "0123456789";
		String especials = "!#$%*()[]";
		String base_pass = null;
		char[] pass_char = null;
		char[] lett_char = null;
		StringBuilder sb = new StringBuilder();
		Random random    = new Random();
		String ret       = "";
		char c;		

		if (p_size <= 0) {
			throw new Exception("getRandomString() has received the invalid size of zero (0) to generate.");
		}

		base_pass = numbers + numbers + letters + numbers + numbers;

		if (p_with_symbols) {
			base_pass = especials + especials + especials + base_pass + especials + especials + especials;
		}

		pass_char = base_pass.toCharArray();
		lett_char = letters.toCharArray();

		//First char will be always a letter.
		sb.append(lett_char[random.nextInt(lett_char.length)]);

		for (int i = 1; i < p_size; i++) {
			c = pass_char[random.nextInt(pass_char.length)];
		    sb.append(c);
		}

		ret = sb.toString();
		return ret;
	}
	
	
	public static Calendar convertTimestampToCalendar (Object obj) {
		try {
			Date date= null;
			
			if (obj == null) {
				LOG.error("Parametro [obj=" + obj + "] em OCTUtils.convertTimestampToCalendar informado é nulo!");
				date = new Date(1);
			}
			
			if (obj instanceof String) {
				if ("".equalsIgnoreCase((String) obj)) {
					return DatatypeFactory.newInstance().newXMLGregorianCalendar().toGregorianCalendar().getInstance();
				}
				
				String tmp = (String) obj;
				tmp = tmp.replaceAll("\\[Timestamp\\=", "");
				tmp = tmp.replaceAll("\\]", "");				
				
				date = new Date(Long.valueOf(tmp) * 1000);
			}
			if (obj instanceof Long) {
				date = new Date((Long) obj * 1000);
			}
			
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			
			GregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c).toGregorianCalendar();
			
			return calendar;
		} catch (Exception e) {
			LOG.error("Erro convertendo [obj=" + obj + "] em GregorianCalendar [" + e.getMessage() + "]", e);
			return null;
		}
	}	
	
	/**
	 * M�todo utilizado para converter as datas retornadas pelo Remedy no formato esperado pelos Services da VPI
	 * @param obj
	 * @return
	 */
	public static String convertTimestampToSDFDate (Object obj, String formatoData) {
		try {
			Date date= null;
			SimpleDateFormat sdf = new SimpleDateFormat(formatoData);
			
			if (obj == null) {
				return null;
			}
			
			if (obj instanceof String) {
				if ("".equalsIgnoreCase((String) obj)) {
					return sdf.format(Calendar.getInstance().getTime());
				}
				
				String tmp = (String) obj;
				if(tmp.contains("Timestamp")){
					tmp = tmp.replaceAll("\\[Timestamp\\=", "");
					tmp = tmp.replaceAll("\\]", "");				
					
					date = new Date(Long.valueOf(tmp) * 1000);
				} else {
					date = sdf.parse(tmp);
				}

			}
			if (obj instanceof Long) {
				date = new Date((Long) obj * 1000);
			}
			
			String format = sdf.format(date);

			return format;
		} catch (Exception e) {
			LOG.error("Erro convertendo [obj=" + obj + "] em SimpleDateFormatData [" + e.getMessage() + "]", e);
			return null;
		}
	}
	
	
	/**
	 * M�todo utilizado para converter Date para o formato esperado pelos Services da VPI
	 * @param obj
	 * @return
	 */
	public static XMLGregorianCalendar convertDateToXmlGregorianCalendar (Date obj) {
		try {			
			if (obj == null) {
				return null;
			}
			
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(obj);
			
			XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			
			return calendar;
		} catch (Exception e) {
			LOG.error("Erro convertendo [date=" + obj.toGMTString() + "] em XMLGregorianCalendar [" + e.getMessage() + "]", e);
			return null;
		}
	}	

	public static String getUniqueDateIdentifier () {
		Date data = new Date();
		SimpleDateFormat dt = new SimpleDateFormat("yyyyymmddhhmmssSSS");
		return dt.format(data);
	}

	public static String formatValueFromRemedy (Object obj) {
		if (obj == null) {
			return "";
		} else {
			return (String) obj;
		}
	}

	public static String generateUUID () {
		return UUID.randomUUID().toString(); 
	}

	public static void setProxyConfig() {
		if (Configuration.getConfig("system.webproxy.enabled").equalsIgnoreCase("true")) {
			System.setProperty("http.proxyHost",     Configuration.getConfig("system.webproxy.http.host"));
	        System.setProperty("http.proxyPort",     Configuration.getConfig("system.webproxy.http.port"));
	        System.setProperty("http.nonProxyHosts", Configuration.getConfig("system.webproxy.http.nonProxyHosts"));
	        System.setProperty("https.proxyHost",    Configuration.getConfig("system.webproxy.https.host"));
	        System.setProperty("https.proxyPort",    Configuration.getConfig("system.webproxy.https.port"));
	        System.setProperty("ftp.proxyHost",      Configuration.getConfig("system.webproxy.ftp.host"));
	        System.setProperty("ftp.proxyPort",      Configuration.getConfig("system.webproxy.ftp.port"));
	        System.setProperty("ftp.nonProxyHosts",  Configuration.getConfig("system.webproxy.ftp.nonProxyHosts"));

		}
	}
	
	public static String cleanString(String p_str) {
		String ret    = "";
		String phase1 = Normalizer.normalize(p_str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		ret           = phase1.replaceAll("[^a-zA-Z0-9 .:-]", "#");
		return ret;
	}
	
	
	
}
