package br.com.ia.oct.mcd.common.utils;

public enum MsgBarramentoEnum {
	
	SITE_OFFLINE(1, "VCIBUS0002E"),
	SITE_NAO_CADASTRADO(2, "ARC00001SE"),
	CATEGORIA(3, "ARC00001CE");
	
	Integer codigo;
	String descricaoErro;
	
	
	private MsgBarramentoEnum(Integer codigo, String descricaoErro) {
		this.codigo = codigo;
		this.descricaoErro = descricaoErro;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescricaoErro() {
		return descricaoErro;
	}
	public void setDescricaoErro(String descricaoErro) {
		this.descricaoErro = descricaoErro;
	}
	
	
	
}
